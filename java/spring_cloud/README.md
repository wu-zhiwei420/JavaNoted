# SpringCloud 笔记

+ [概述](java/spring_cloud/概述.md)
+ [服务注册与发现](java/spring_cloud/服务注册与发现.md)
+ [微服务熔断机制与Hystrix原理](java/spring_cloud/微服务熔断机制与Hystrix原理.md)
+ [Zuul 网关路由](java/spring_cloud/Zuul_网关路由.md)
+ [Ribbon 客户端负载均衡](java/spring_cloud/Ribbon_客户端负载均衡.md)
