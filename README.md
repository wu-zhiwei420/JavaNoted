个人学习笔记，主要是自己近些年的学习内容记录以及面试的知识点，同时会记录一些最新的观点和问题，文章同步发布在 [个人博客](https://www.zhoutao123.com)
以及 [语雀文档](https://www.yuque.com/zhoutao123) 如果您对我的文章感兴趣，欢迎关注，如果文章对您有帮助的话，欢迎 Star支持⭐️ 或者提交 PR 🔀 ，您的支持是我不断更新的动力~

---

[《 深入理解 Java 虚拟机》](https://www.zhoutao123.com/page/book/1) [《 后端架构设计》](https://www.zhoutao123.com/page/book/2) [《 Java 基础知识进阶》](https://www.zhoutao123.com/page/book/3)  [《 Nginx 学习笔记》](https://www.zhoutao123.com/page/book/4)  [《 前端开发杂记》](https://www.zhoutao123.com/page/book/5)  [《 设计模式学习笔记》](https://www.zhoutao123.com/page/book/6)  [《 DevOps 最佳实践指南》](https://www.zhoutao123.com/page/book/7)  [《 Netty 入门与实战》](https://www.zhoutao123.com/page/book/8)  [《 高性能MYSQL》](https://www.zhoutao123.com/page/book/9)  [《 JavaEE 常用框架》](https://www.zhoutao123.com/page/book/10)  [《 Java 并发编程学习笔记》](https://www.zhoutao123.com/page/book/11)  [《 分布式系统》](https://www.zhoutao123.com/page/book/12)  [《 数据结构与算法》](https://www.zhoutao123.com/page/book/13)

项目目前涵盖了: Java、JVM、Java并发、DevOps、设计模式、架构设计、各种中间件入门以及原理等等，因个人 能力有限，如果文档有错误，欢迎指出，非常感谢

---

## 链接
+ ✏️ [燕归来兮-个人博客(最快更新)](https://www.zhoutao123.com)
+ 🤤 [Gitee(访问更快)](https://gitee.com/taoes_admin/JavaNoted) 
+ ⚠️ [Github](https://github.com/taoes/JavaNoted)



## 📖 目录

### 🔥 JavaWeb 相关框架

+ [Spring 源码分析](./java/spring/README.md)
+ [SpringBoot 注解源码分析](./java/spring_boot/README.md)
+ [Spring Cloud 组件与原理](./java/spring_cloud/README.md)
+ [MyBatis 入门及原理](./java/mybatis/README.md)

### 🔥 JavaSE

+ [Java虚拟机](./java/jvm/README.md)
+ [Java 并发编程](./java/concurrent/README.md)

### ✨其他

+ [分布式系统](./java/distributed/README.md)
  


## 🧑🏻‍💼 面试
+ [Java面试知识点汇总](./interview)




